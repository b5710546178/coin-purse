package coinpurse;
 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 */
public class Main {

    /**
     * @param args not used
     */
    public static void main( String[] args ) {
//TODO follow the steps in the sequence diagram
        // 1. create a Purse
    	Purse haha = new Purse(4);
    	Coin kik = new Coin(32);
        // 2. create a ConsoleDialog with a reference to the Purse object
    	ConsoleDialog hah = new ConsoleDialog(haha);
        // 3. run() the ConsoleDialog
    	hah.run();
    }
}
