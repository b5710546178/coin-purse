package coinpurse;

import java.util.Hashtable;
import java.util.Map;

public class Coupon extends AbstractValuable implements Valuable{
	/**
	 * A coupon with a monetary value. You can't change the value of a coupon.
	 * 
	 * @author Kwanwan Tantichartkul
	 */
	
	/** Color of banknote. */
	private String color;

	/** Map the value of banknote in each color */
	Map<String,Double>map = new Hashtable<String,Double>( );
	/**
	 * Constructor for a new coupon.
	 * 
	 * @param color is the color of coupon
	 */
	public Coupon(String color){
		this.color = color;
		map.put("red",100.0);
		map.put("blue",50.0);
		map.put("green",20.0);
	}
	/**
	 * @return value of coupon
	 */
	public double getValue(){
		return map.get(color);
	}
	/**
	 * @return String of coupon
	 */
	public String toString(){
		return color + "  coupon";
	}
}