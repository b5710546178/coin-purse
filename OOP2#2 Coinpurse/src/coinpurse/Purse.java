package coinpurse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Collection;
import java.util.List;


/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  
 *  @author your name
 */
public class Purse{
    /** Collection of coins in the purse. */
   ValueComparator Comparator = new ValueComparator();
    private ArrayList<Valuable> Moneys;
    /** Capacity is maximum NUMBER of coins the purse can hold.
     *  Capacity is set when the purse is created.
     */
    private int capacity;
    
    /** 
     *  Create a purse with a specified capacity.
     *  @param capacity is maximum number of coins you can put in purse.
     */
    public Purse( int capacity ) {
	//TODO initialize the attributes of purse
    	this.capacity = capacity;
    	Moneys = new ArrayList<Valuable>();
    }

    /**
     * Count and return the number of coins in the purse.
     * This is the number of coins, not their value.
     * @return the number of coins in the purse
     */
    public int count() { 
    	
    	return Moneys.size(); 
    	}
    
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    public double getBalance() {
    	double balance =0;
    	for(int i=0;i<Moneys.size();i++){
    		balance += Moneys.get(i).getValue();
    	}
    	return balance; 
    }

    
    /**
     * Return the capacity of the coin purse.
     * @return the capacity
     */
    
    public int getCapacity() { 
    	return this.capacity; 
    	}
    
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    public boolean isFull() {
        
    	if(Moneys.size()==this.capacity)
    		return true;
    	else 
    		return false;
    }

    /** 
     * Insert a coin into the purse.
     * The coin is only inserted if the purse has space for it
     * and the coin has positive value.  No worthless coins!
     * @param money is a Coin object to insert into purse
     * @return true if coin inserted, false if can't insert
     */
    public boolean insert( Valuable money ) {
        // if the purse is already full then can't insert anything.
        //TODO complete the insert method
    	if(isFull()){
    		return false;
    	}
    	else{
    		Moneys.add(money);
    		return true;
    	}
    }
    
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of Coins withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
    public Valuable[] withdraw( double amount ) {
        //TODO don't allow to withdraw amount < 0
        //if ( ??? ) return ???;
    	
    	if ( amount < 0 )
		{	
			return null;
		}
    	
		
//TODO code for withdraw
    	List<Valuable> collect = new ArrayList<Valuable>();

		// Did we get the full amount?
		if ( amount > 0 )
		{	// failed. Since you haven't actually remove
			// any coins from Purse yet, there is nothing
			// to put back.
			
			
			
	        for(int i=Moneys.size()-1;i>=0;i--){
	        	if(Moneys.get(i).getValue()<=amount){
	        		collect.add(Moneys.get(i));
	        		amount-=Moneys.get(i).getValue();
	   
	        	}
	        }
	        Collections.sort(collect,Comparator);
	        for(int k=0;k<collect.size();k++){
	        	for(int j =0;j<Moneys.size();j++){
	        		if(Moneys.get(j).getValue()==collect.get(k).getValue())
	        			Moneys.remove(j);
	        	}
	        	
	        }
		}

		 Valuable [] Coints ={};
	        Coints = collect.toArray(Coints);
	        return Coints;
		
	}
  
    /** 
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     */
    public String toString() {

    	return Moneys.size()+" coins with value "+ getBalance();
    }

}

