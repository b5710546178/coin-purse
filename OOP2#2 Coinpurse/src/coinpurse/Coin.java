package coinpurse;
 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author your name
 */
public class Coin extends AbstractValuable implements Comparable,Valuable{

    /** Value of the coin */
    private double value;
    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value ) {
        this.value = value;
       
    }
    /** 
     * @param Get value of coins.
     * return value of coins
     */

//TODO Write a getValue() method.
    public double getValue(){
    	return this.value;
    }
    /** 
     * @param Compare value of coins.
     * return -1 if o equals null and value less than other value.
     * return 0 if value equals other value.
     * return 1 if  value more than other value.
     */
@Override
public int compareTo(Object o) {
	if ( o==null ) 
		return -1;
	Coin other = (Coin) o;
		if ( value < other.value ) 
			return -1;
		if ( value == other.value )
			return 0;
		else 
			return 1;
}

/** 
 * return String of value of money.
 */
public String toString(){
	return (int)value + " -Baht coin";
}
    
}

