package coinpurse;

public abstract class AbstractValuable implements Valuable {
	/**
	 * AbstractValuable is the superclass of Coin BankNote and Coupon.
	 * 
	 * @author Kwanwan Tantichartkul
	 */
	public AbstractValuable() {
		super();
	}
	public abstract double getValue();
	/**
	 * compare two valuables.
	 * @param obj other valuable that want to compare
	 * @return <0 if first value is less than the second,
	 * 		   >0 if first value is more than the second,
	 *         =0 if both value is equal
	 */
	public int compareTo(Valuable obj){
		if(obj == null) return -1;
		return (int) Math.signum(this.getValue() - obj.getValue());
	}
	
	/**
	 * check that two objects are the same type and they are equal.
	 * @param obj another that will be checked
	 * @return true if two objects are the same type and are equal, otherwise return false.
	 */
	public boolean equals(Object obj) {
		if (obj == null) 
			return false;
		else if ( obj.getClass() != Coin.class ){
			Coin other = (Coin) obj;
		return this.getValue()==other.getValue();
		}
		else if ( obj.getClass() != BankNote.class ){
			BankNote other = (BankNote) obj;
			return this.getValue()==other.getValue();
		}
		else if ( obj.getClass() != Coupon.class ){
			Coupon other = (Coupon) obj;
			return this.getValue()==other.getValue();
		}
		
	return false;
	}

}