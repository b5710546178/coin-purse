package coinpurse;

import java.util.Comparator;

public class ValueComparator implements Comparator<Valuable> {
public int compare(Valuable a, Valuable b) {
// compare them by value. This is easy.
	if(a.getValue()>b.getValue())
		return -1;
	else if(a.getValue()<b.getValue())
		return 1;
	else 
		return 0;
}

}