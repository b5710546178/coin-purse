package coinpurse;

public class BankNote extends AbstractValuable implements Valuable{
	double value;
	private static double serialNumber=1000000;
	
	public BankNote(double value){
		this.value = value;
		this.getNextSerialNumber();
	}
	
	public double getValue(){
		return this.value;
	}
	
	public double getNextSerialNumber(){
		return this.serialNumber++;
	}
	
	public String toString(){
		return (int)value + "-Baht Banknote ["+ (int)getNextSerialNumber() + "]";
	}
}